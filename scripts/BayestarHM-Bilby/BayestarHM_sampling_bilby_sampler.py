import lal
import time
import bilby
import emcee
import pycbc
import corner

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pycbc.waveform as wf
import lalsimulation as lalsim

from scipy.interpolate import interp1d
from pycbc.detector import Detector
from collections import defaultdict
from bilby.core.likelihood import Likelihood


class PSD:
    def __init__(self, psd_filename, prepend_points = [], append_points = []):
        self.f_vals, self.psd_vals = np.loadtxt(psd_filename, unpack = True)
        self.prepend_points = prepend_points
        self.append_points = append_points

        for point in self.prepend_points:
            self.f_vals = np.concatenate([np.array([point[0]]), self.f_vals])
            self.psd_vals = np.concatenate([np.array([point[1]]), self.psd_vals])

        for point in self.append_points:
            self.f_vals = np.concatenate([self.f_vals, np.array([point[0]])])
            self.psd_vals = np.concatenate([self.psd_vals, np.array([point[1]])])

        self.psd = interp1d(self.f_vals, self.psd_vals)

ligo_H1_psd = '/home1/mukesh.singh/work/EW/data/GW170814/H1_PSD_GW170814.txt'
ligo_L1_psd = '/home1/mukesh.singh/work/EW/data/GW170814/L1_PSD_GW170814.txt'
V1_psd = '/home1/mukesh.singh/work/EW/data/GW170814/V1_PSD_GW170814.txt'

H1_psd_int = PSD(ligo_H1_psd)
L1_psd_int = PSD(ligo_L1_psd)
V1_psd_int = PSD(V1_psd)


def spher_harms_plus_cross_factors(inclination, l, m):
    
    Y_lm = lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, m)
    Y_lm_star = np.conj(lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, -m))
    
    if (l % 2):
        minus1l = -1
    else:
        minus1l = 1
        
    C_lm =  ( Y_lm + minus1l * Y_lm_star)
    D_lm = -1j * ( Y_lm - minus1l * Y_lm_star)
    
    return C_lm, D_lm

def waveform_lm_mode(fmin, fmax, df, mode, m1, m2, inclination, distance, approx):
    
    mode_array_dict = {
        "22": [[2, 2], [2, -2]],
        "32": [[3, 2], [3, -2]],
        "21": [[2, 1], [2, -1]],
        "44": [[4, 4], [4, -4]],
        "33": [[3, 3], [3, -3]],
        "43": [[4, 3], [4, -3]]
    }
    
    hp, hc = wf.get_fd_waveform(approximant= approx,
                                mass1 = m1, 
                                mass2 = m2, 
                                inclination= inclination,
                                distance = distance,
                                f_lower = fmin,
                                f_final = fmax,
                                delta_f = df,
                                mode_array = mode_array_dict[mode]
                                #coa_phase = coa_phase)
                               )
    # remove the angular dependence through dividing by spherical harmonics
    # so that we have to evaluate instrinsic part only once
    l = int(mode[0])
    m = int(mode[1])
    C_lm, D_lm = spher_harms_plus_cross_factors(inclination, l, m)
    freq = hp.sample_frequencies.data
    hp = hp.data/C_lm
    hc = hc.data/(D_lm)
    h_intrinsic = hp
    return freq, h_intrinsic, hc

# ML estimates for GW170814
mle = dict()
mle["ra"] = 47.75*np.pi/180
mle["dec"] = -np.pi/4
mle["psi"] = 0.0
mle["phi0"] = 0.0
mle["m1"] = 30.6
mle["m2"] = 25.2
mle["theta_jn"] = np.pi/4
mle["coa_phase"] = np.pi/4
mle["distance"] = 600.
mle["approx"] = "IMRPhenomXPHM"
mle["geocent_time"] = 1186741861.0

def make_data(f_min, f_max, df, params, modes, detectors):
    data = {}
    h_intrinsic = {}
    signal = {}
    mle = params
    for det in detectors:
        signal[det] = 0.0
        Det = Detector(det)
        dt =  Det.time_delay_from_earth_center(mle["ra"], mle["dec"], mle["geocent_time"])
        t_det =  mle["geocent_time"] + dt
        fp, fc = Det.antenna_pattern(mle["ra"], mle["dec"], mle["psi"], mle["geocent_time"])
        for mode in modes:
            h_intrinsic[mode] = 0.0
            f, h_intrinsic[mode], _ = waveform_lm_mode(f_min, f_max, df, mode, mle["m1"], mle["m2"], mle["theta_jn"], mle["distance"], mle["approx"])
            l = int(mode[0])
            m = int(mode[1])
            
            lower_index = f >= f_min 
            f = f[lower_index]
            h_intrinsic[mode] = h_intrinsic[mode][lower_index]

            upper_index = f <= f_max
            f = f[upper_index]
            h_intrinsic[mode] = h_intrinsic[mode][upper_index]

            C_lm, D_lm = spher_harms_plus_cross_factors(mle["theta_jn"], l, m)
            phase_shift = (np.cos(2*np.pi*f*t_det + m*mle["phi0"]) + 1j*np.sin(2*np.pi*f*t_det + m*mle["phi0"]))
            signal[det] += (fp*C_lm + fc*D_lm)*h_intrinsic[mode]*phase_shift
    data["frequency_array"] = f
    data["signal"] = signal
    data["intrinsic_part"] = h_intrinsic
    return data


modes = ["22"]
detectors = ["H1", "L1", "V1"]
fmin = 20
fmax = 1000
df = 0.1
data = make_data(fmin, fmax, df, mle, modes, detectors)
f_array = data["frequency_array"]

H1_psd = H1_psd_int.psd(f_array)
L1_psd = L1_psd_int.psd(f_array)
V1_psd = V1_psd_int.psd(f_array)

def bayestarhm_source_model(frequency_array, intrinsic_signal, det, kwargs, ra, dec, inclination, distance,  psi, geocent_time, phi0):
    
    Det = Detector(det)
    
    dt = Det.time_delay_from_earth_center(ra, dec, geocent_time)
    t_det =  geocent_time + dt 
    fp, fc = Det.antenna_pattern(ra, dec, psi, geocent_time)
    
    signal_model = 0.0
    phase_shift_t_det = np.cos(2*np.pi*frequency_array*t_det) + 1j*np.sin(2*np.pi*frequency_array*t_det)
    for mode in kwargs["modes"]:
        l = int(mode[0])
        m = int(mode[1])
        C_lm, D_lm = spher_harms_plus_cross_factors(inclination, l, m)
        phase_shift = phase_shift_t_det*(np.cos(m*phi0) + 1j*np.sin(m*phi0))
        signal_model += (fp*C_lm + fc*D_lm)*intrinsic_signal[mode]*phase_shift
            
    return signal_model*(mle["distance"]/distance)


class BayestarHMLikelihood(bilby.Likelihood):
    def __init__(self, f, data, model, params, detectors, modes):
        
        self.f = f
        self.data = data
        self.N = len(f)
        self.model = model
        self.parameters = params
        self.detectors = detectors
        self.modes = modes
        
        self._meta_data = None        
        self._marginalized_parameters = []
        
    def log_likelihood(self):

        waveform_kwargs = {}
        waveform_kwargs["modes"] = self.modes
        frequency_array = self.f
        log_l = 0.0

        #if condition plays the role of prior
        for det in self.detectors:
            template = self.model(frequency_array, self.data["intrinsic_part"], det, waveform_kwargs, **self.parameters)
            likelihood_integrand = abs(self.data["signal"][det] - template)**2
            psd = eval('%s_psd'%(det))
            log_l += -2*np.trapz(likelihood_integrand/psd, frequency_array) 
        return log_l         

#setting up bilby priors
priors = dict(ra=bilby.core.prior.Uniform(0, 6.283185307179586, name='ra', latex_label=r"$\alpha$", unit="rad", boundary="periodic"),
              dec=bilby.core.prior.Cosine(minimum=-1.5707963267948966,maximum=1.5707963267948966, name='dec', latex_label=r"$\delta$", unit="rad", boundary="periodic"),
              inclination=bilby.core.prior.Sine(minimum=0, maximum=3.141592653589793, name='theta_jn', latex_label=r"$\theta_{\mathrm{JN}}$", unit="rad", boundary="periodic"),
              distance=bilby.gw.prior.UniformComovingVolume(100, 1200, name='luminosity_distance', latex_label="$d_L$", unit="Mpc"),
              psi=bilby.core.prior.Uniform(0, 6.283185307179586, name='psi', latex_label=r"$\psi$", unit="rad", boundary="periodic"),
              geocent_time=bilby.core.prior.Uniform(mle["geocent_time"]-0.1, mle["geocent_time"]+0.1, name="geocent_time", latex_label=r'$t_{\mathrm{geocent_time}}$', unit="secs"),
              phi0=bilby.core.prior.Uniform(0, 6.283185307179586, name='phi0', latex_label=r"$\varphi_{0}$", unit="rad", boundary="periodic"))

outdir = 'all_extrinsic_parameters_non_uniform_priors'
livepoints = 1000
walks = 100

parameters = dict(ra=mle["ra"], dec=mle["dec"], inclination=mle["theta_jn"],\
                  distance=mle["distance"], psi=mle["psi"], geocent_time=mle["geocent_time"],\
                  phi0=mle["phi0"])

BayestarHM_likelihood = BayestarHMLikelihood(f = f_array, data = data, model = bayestarhm_source_model, params=parameters, detectors=detectors, modes=modes)
result = bilby.run_sampler(
    likelihood=BayestarHM_likelihood, priors=priors, sampler='dynesty', npoints=livepoints,
    walks=walks, outdir=outdir, label='BayestarHM_all_extrinsic_params_with_non_trivial_priors_and_delay_time_nlive_1000')

result.plot_corner(truth=dict(ra=mle["ra"], dec = mle["dec"], inclination=mle["theta_jn"],\
                  distance=mle["distance"], psi=mle["psi"], geocent_time=mle["geocent_time"],\
                  phi0=mle["phi0"]), titles = True, save=True)
result.plot_marginals(parameters=parameters, quantiles=[0.05, 0.95])

