
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import pycbc
import pycbc.waveform as wf
from pycbc.detector import Detector
import lalsimulation as lalsim
import lal
from collections import defaultdict
import emcee
import time
import corner
from dynesty import NestedSampler

class PSD:
    def __init__(self, psd_filename, prepend_points = [], append_points = []):
        self.f_vals, self.psd_vals = np.loadtxt(psd_filename, unpack = True)
        self.prepend_points = prepend_points
        self.append_points = append_points

        for point in self.prepend_points:
            self.f_vals = np.concatenate([np.array([point[0]]), self.f_vals])
            self.psd_vals = np.concatenate([np.array([point[1]]), self.psd_vals])

        for point in self.append_points:
            self.f_vals = np.concatenate([self.f_vals, np.array([point[0]])])
            self.psd_vals = np.concatenate([self.psd_vals, np.array([point[1]])])

        self.psd = interp1d(self.f_vals, self.psd_vals)

ligo_H1_psd = '/home/mukesh/Academics/EW3/data/GW170814/H1_PSD_GW170814.txt'
ligo_L1_psd = '/home/mukesh/Academics/EW3/data/GW170814/L1_PSD_GW170814.txt'
V1_psd = '/home/mukesh/Academics/EW3/data/GW170814/V1_PSD_GW170814.txt'

H1_psd_int = PSD(ligo_H1_psd)
L1_psd_int = PSD(ligo_L1_psd)
V1_psd_int = PSD(V1_psd)



def spher_harms_plus_cross_factors(inclination, phi0, l, m):
    
    d_lm = lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, m).real
    d_lminusm = lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, -m).real
    Y_lm = lal.SpinWeightedSphericalHarmonic(inclination, phi0, -2, l, m).real
    
    #Notations as deifined in the notes
    C_lm = (((-1)**l)*(d_lminusm/d_lm) + 1)*Y_lm
    D_lm = (((-1)**l)*(d_lminusm/d_lm) - 1)*Y_lm
    
    return C_lm, D_lm


def waveform_lm_mode(fmin, fmax, df, mode, m1, m2, inclination, distance, coa_phase, approx):
    
    mode_array_dict = {
        "22": [[2, 2], [2, -2]],
        "32": [[3, 2], [3, -2]],
        "21": [[2, 1], [2, -1]],
        "44": [[4, 4], [4, -4]],
        "33": [[3, 3], [3, -3]],
        "43": [[4, 3], [4, -3]],
        #"2244": [[2, 2], [4, 4], [4, -4]],
        #"2233": [[2, 2], [3, 3], [3, -3]],
    }
    
    hp, hc = wf.get_fd_waveform(approximant= approx,
                                mass1 = m1, 
                                mass2 = m2, 
                                inclination= inclination,
                                distance = distance,
                                f_lower = fmin,
                                f_final = fmax,
                                delta_f = df,
                                mode_array = mode_array_dict[mode],
                                coa_phase = coa_phase)
    
    # remove the angular dependence through dividing by spherical harmonics
    # so that we have to evaluate instrinsic part only once
    l = int(mode[0])
    m = int(mode[1])
    C_lm, D_lm = spher_harms_plus_cross_factors(inclination, 0.0, l, m)
    freq = hp.sample_frequencies.data
    hp = hp.data/C_lm
    hc = hc.data/(-1j*D_lm)
    h_intrinsic = hp
    return freq, h_intrinsic #, hp*C_lm

def antenna_pattern_eff(ra, dec, pol, gps_time, det):
    # We can calcualate the antenna pattern for Hanford at
    # the specific sky location
    d = Detector(det)

    # We get back the fp and fc antenna pattern weights.
    fp, fc = d.antenna_pattern(ra, dec, pol, gps_time)
    #print("fp={}, fc={}".format(fp, fc))
    f_mod = np.sqrt(fp**2 + fc**2)
    
    if det == "H1":
        kappa = np.arccos(fp/f_mod) - np.pi/2
    else:
        kappa = np.arccos(fp/f_mod)
    
    return kappa, f_mod

# ML estimates for GW170814
mle = dict()
mle["ra"] = 47.75*np.pi/180
mle["dec"] = -np.pi/4
#mle["kappa"] = np.pi/4.
mle["psi"] = np.pi/3.
mle["m1"] = 30.6
mle["m2"] = 25.2
mle["theta_jn"] = np.pi/4
mle["coa_phase"] = np.pi/4
mle["distance"] = 600.
mle["approx"] = "IMRPhenomXPHM"
mle["geocent_time"] = 1186741861.0
modes = ["22", "33", "44"]

h_intrinsic = {}
signal = {}
f_min = 20.
f_max = 1000.
df = 0.01
detectors = ["H1", "L1", "V1"]
for det in detectors:
    signal[det] = 0.0
    kappa, f_mod = antenna_pattern_eff(mle["ra"], mle["dec"], mle["psi"], mle["geocent_time"], det)
    for mode in modes:
        h_intrinsic[mode] = 0.0
        f, h_intrinsic[mode] = waveform_lm_mode(f_min, f_max, df, mode, mle["m1"], mle["m2"], mle["theta_jn"], mle["distance"], mle["coa_phase"], mle["approx"])
        l = int(mode[0])
        m = int(mode[1])
        lower_index = f >= f_min
        f = f[lower_index]
        h_intrinsic[mode] = h_intrinsic[mode][lower_index]
    
        upper_index = f <= f_max
        f = f[upper_index]
        h_intrinsic[mode] = h_intrinsic[mode][upper_index]
    
        C_lm, D_lm = spher_harms_plus_cross_factors(mle["theta_jn"], 0.0, l, m)
        signal[det] += (C_lm*np.cos(kappa) -1j*D_lm*np.sin(kappa))*h_intrinsic[mode]
    signal[det] = signal[det]*f_mod

H1_psd = H1_psd_int.psd(f)
L1_psd = L1_psd_int.psd(f)
V1_psd = V1_psd_int.psd(f)

def bayestarhm_source_model(intrinsic_signal, inclination, distance, ra, dec, psi, geocent_time, det, kwargs):
    
    #evaluates only extrinsic part of the waveform
    #Intrinsic part of the signal of every mode should be provided as a dictionary  
    kappa, f_modulus = antenna_pattern_eff(ra, dec, psi, geocent_time, det)
    cos_kappa = np.cos(kappa)
    sin_kappa = np.sin(kappa)
    
    signal = 0.0
    
    for mode in kwargs["modes"]:
        l = int(mode[0])
        m = int(mode[1])
        C_lm, D_lm = spher_harms_plus_cross_factors(inclination, 0.0, l, m)
        signal += (C_lm*cos_kappa -1j*D_lm*sin_kappa)*intrinsic_signal[mode]
            
    return signal*f_modulus

def log_bayestarhm_like(params_vec):
    
    waveform_kwargs = {}
    waveform_kwargs["modes"] = ["22", "33", "44"]
    frequency_array = f
    log_likelihood = 0.0
    for det in detectors:
            model = bayestarhm_source_model(h_intrinsic, params_vec[0], params_vec[1], params_vec[2], params_vec[3], params_vec[4], params_vec[5], det, waveform_kwargs)
            likelihood_integrand = abs(signal[det] - model)**2
            psd = eval('%s_psd'%(det))
            log_likelihood += -2*np.trapz(likelihood_integrand/psd, frequency_array) + params_vec[1]**2
    return log_likelihood


def prior_transform(u):
    
    x = np.array(u)
    
    x[0] = np.arccos(u[0] * 2. - 1.)
    
    x[1] = 500*u[1] + 300
    
    x[2] = 2*np.pi*u[2]
    
    x[3] = np.arcsin(u[3]*2 - 1)
    
    x[4] = 2*np.pi*u[4]
    
    x[5] = mle["geocent_time"] + (u[5]*0.2 - 0.1)
    
    return x

ndim=6
nact=20
nlive=2000
sampler = NestedSampler(log_bayestarhm_like, prior_transform, ndim, nlive=nlive, nact=nact, bound='none', sample='rwalk')

sampler.run_nested(dlogz=0.1)
dataframe = pd.DataFrame(sampler.results["samples"])
dataframe.to_csv("nested_sampling_run_iota_dL_ra_dec_psi_geocent_time_nact_%d_nlive_%.dat"%(nact_value, nlive), sep=" ", header=["theta_jn", "luminosity_distance", "ra", "dec", "psi", "geocent_time"], index=False)