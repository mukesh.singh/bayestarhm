
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import pycbc
import pycbc.waveform as wf
from pycbc.detector import Detector
import lalsimulation as lalsim
import lal
from collections import defaultdict
import emcee
import time
import corner
import pandas as pd

class PSD:
    def __init__(self, psd_filename, prepend_points = [], append_points = []):
        self.f_vals, self.psd_vals = np.loadtxt(psd_filename, unpack = True)
        self.prepend_points = prepend_points
        self.append_points = append_points

        for point in self.prepend_points:
            self.f_vals = np.concatenate([np.array([point[0]]), self.f_vals])
            self.psd_vals = np.concatenate([np.array([point[1]]), self.psd_vals])

        for point in self.append_points:
            self.f_vals = np.concatenate([self.f_vals, np.array([point[0]])])
            self.psd_vals = np.concatenate([self.psd_vals, np.array([point[1]])])

        self.psd = interp1d(self.f_vals, self.psd_vals)

ligo_H1_psd = '/home/mukesh/Academics/EW3/data/GW170814/H1_PSD_GW170814.txt'
ligo_L1_psd = '/home/mukesh/Academics/EW3/data/GW170814/L1_PSD_GW170814.txt'
V1_psd = '/home/mukesh/Academics/EW3/data/GW170814/V1_PSD_GW170814.txt'

H1_psd_int = PSD(ligo_H1_psd)
L1_psd_int = PSD(ligo_L1_psd)
V1_psd_int = PSD(V1_psd)

def spher_harms_plus_cross_factors(inclination, l, m):
    
    Y_lm = lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, m)
    Y_lm_star = np.conj(lal.SpinWeightedSphericalHarmonic(inclination, 0, -2, l, -m))
    
    if (l % 2):
        minus1l = -1
    else:
        minus1l = 1
        
    C_lm =  ( Y_lm + minus1l * Y_lm_star)
    D_lm = -1j * ( Y_lm - minus1l * Y_lm_star)
    
    return C_lm, D_lm


def waveform_lm_mode(fmin, fmax, df, mode, m1, m2, inclination, distance, approx):
    
    mode_array_dict = {
        "22": [[2, 2], [2, -2]],
        "32": [[3, 2], [3, -2]],
        "21": [[2, 1], [2, -1]],
        "44": [[4, 4], [4, -4]],
        "33": [[3, 3], [3, -3]],
        "43": [[4, 3], [4, -3]]
    }
    
    hp, hc = wf.get_fd_waveform(approximant= approx,
                                mass1 = m1, 
                                mass2 = m2, 
                                inclination= inclination,
                                distance = distance,
                                f_lower = fmin,
                                f_final = fmax,
                                delta_f = df,
                                mode_array = mode_array_dict[mode]
                                #coa_phase = coa_phase)
                               )
    # remove the angular dependence through dividing by spherical harmonics
    # so that we have to evaluate instrinsic part only once
    l = int(mode[0])
    m = int(mode[1])
    C_lm, D_lm = spher_harms_plus_cross_factors(inclination, l, m)
    freq = hp.sample_frequencies.data
    hp = hp.data/C_lm
    hc = hc.data/(D_lm)
    h_intrinsic = hp
    return freq, h_intrinsic, hc



# ML estimates for GW170814
mle = dict()
mle["ra"] = 47.75*np.pi/180
mle["dec"] = -np.pi/4
mle["psi"] = 0.0
mle["phi0"] = 0.0
mle["m1"] = 30.6
mle["m2"] = 25.2
mle["theta_jn"] = np.pi/4
mle["coa_phase"] = np.pi/4
mle["distance"] = 600.
mle["approx"] = "IMRPhenomXPHM"
mle["geocent_time"] = 1186741861.0
modes = [ "22"]#, "33", "44"]

h_intrinsic = {}
signal = {}
f_min = 20.
f_max = 1000.
df = 0.1
detectors = ["H1", "L1", "V1"]
for det in detectors:
    signal[det] = 0.0
    Det = Detector(det)
    dt =  Det.time_delay_from_earth_center(mle["ra"], mle["dec"], mle["geocent_time"])
    t_det = mle["geocent_time"] - dt
    fp, fc = Det.antenna_pattern(mle["ra"], mle["dec"], mle["psi"], mle["geocent_time"])
    print("fplus = %f, fcross = %f for and time at the detector %s = %f "%(fp, fc, det, mle["geocent_time"] - dt))
    for mode in modes:
        h_intrinsic[mode] = 0.0
        f, h_intrinsic[mode], _ = waveform_lm_mode(f_min, f_max, df, mode, mle["m1"], mle["m2"], mle["theta_jn"], mle["distance"], mle["approx"])
        l = int(mode[0])
        m = int(mode[1])
        
        lower_index = f >= f_min
        f = f[lower_index]
        h_intrinsic[mode] = h_intrinsic[mode][lower_index]
    
        upper_index = f <= f_max
        f = f[upper_index]
        h_intrinsic[mode] = h_intrinsic[mode][upper_index]
        
        C_lm, D_lm = spher_harms_plus_cross_factors(mle["theta_jn"], l, m)
        phase_shift = (np.cos(2*np.pi*f*t_det + m*mle["phi0"]) + 1j*np.sin(2*np.pi*f*t_det + m*mle["phi0"]))
        
        signal[det] += (fp*C_lm + fc*D_lm)*h_intrinsic[mode]*phase_shift

H1_psd = H1_psd_int.psd(f)
L1_psd = L1_psd_int.psd(f)
V1_psd = V1_psd_int.psd(f)

def bayestarhm_source_model(intrinsic_signal, inclination, distance, ra, dec, psi, geocent_time, phi0, det, kwargs):
    
    #evaluates only extrinsic part of the waveform
    #Intrinsic part of the signal of every mode should be provided as a dictionary    
    Det = Detector(det)
    dt = Det.time_delay_from_earth_center(ra, dec, geocent_time)
    t_det = geocent_time - dt
    fp, fc = Det.antenna_pattern(ra, dec, psi, geocent_time)
    
    signal_model = 0.0
    phase_shift_t_det = np.cos(2*np.pi*f*t_det) + 1j*np.sin(2*np.pi*f*t_det)
    for mode in kwargs["modes"]:
        l = int(mode[0])
        m = int(mode[1])
        C_lm, D_lm = spher_harms_plus_cross_factors(inclination, l, m)
        phase_shift = phase_shift_t_det*(np.cos(m*phi0) + 1j*np.sin(m*phi0))
        signal_model += (fp*C_lm + fc*D_lm)*intrinsic_signal[mode]*phase_shift
            
    return signal_model*(mle["distance"]/distance)


def log_bayestarhm_like_emcee(params_vec):
    
    waveform_kwargs = {}
    waveform_kwargs["modes"] = ["22"]#, "33", "44"]
    frequency_array = f
    log_likelihood = 0.0
    #print(params_vec)
    if (-1 <= np.cos(params_vec[0]) and np.cos(params_vec[0]) <= 1)*(300 <= params_vec[1] and params_vec[1] < 800 )*(0 <= params_vec[2] and params_vec[2] <=2*np.pi)*(-1 <= np.sin(params_vec[3]) and np.sin(params_vec[3]) <=1)*(0 <= params_vec[4] and params_vec[4] <=2*np.pi)*(mle["geocent_time"]-0.1 <= params_vec[5] and params_vec[5] <= mle["geocent_time"]+0.1)*(0 <= params_vec[6] and params_vec[6] <=2*np.pi) == 1:
        
        for det in detectors:
            model = bayestarhm_source_model(h_intrinsic, params_vec[0], params_vec[1], params_vec[2],                                          params_vec[3], params_vec[4], params_vec[5], params_vec[6], det, waveform_kwargs)
            likelihood_integrand = abs(signal[det] - model)**2
            psd = eval('%s_psd'%(det))
            log_likelihood += -2*np.trapz(likelihood_integrand/psd, frequency_array)
        
        return log_likelihood
    else:
        return -np.inf


n_walkers = 50
ndim = 7
burnin_steps = 1000
steps = 100000
guess_value = np.array([mle["theta_jn"],mle["distance"], mle["ra"], mle["dec"], mle["psi"], mle["geocent_time"], mle["phi0"]])


p0_zero = np.ones([n_walkers,ndim])*guess_value
p0 = p0_zero+np.array([np.random.rand(ndim) for i in range(n_walkers)])

t0 = time.time()
sampler = emcee.EnsembleSampler(n_walkers,ndim,log_bayestarhm_like_emcee)
p1,lnpos1,rs1 = sampler.run_mcmc(p0,burnin_steps)
sampler.reset()
sampler.run_mcmc(p1,steps)
tf = time.time()
print("Time taken in running Emcee sampler with %d walkers and %d steps = %f"%(n_walkers, steps, (tf-t0)))


dataframe = pd.DataFrame(sampler.flatchain)
dataframe.to_csv("22_emcee_sampling_run_iota_dL_ra_dec_psi_geocent_time_phi0_walkers_%d_steps_%d_b_steps_%d.dat"%(n_walkers,steps,burnin_steps), sep=" ", header=["theta_jn", "luminosity_distance", "ra", "dec", "psi", "geocent_time", "phi0"], index=False)